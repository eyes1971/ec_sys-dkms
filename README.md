# ec_sys-dkms

This is a sample project for installing the `ec_sys` kernel module on Linux systems using the DKMS method. The module provides an interface to read and write data in Embedded Controller (EC) Sysfs filesystem.

## Dependencies

To use this software package, you need to install the following dependencies:

- dkms
- build-essential
- linux-headers-\$(uname -r)

### Debian / Ubuntu

You can install these dependencies by running:

```
sudo apt install dkms build-essential linux-headers-\$(uname -r)
```

### Fedora

You can install these dependencies by running:

```
sudo dnf install kernel-devel dkms make
```

## About nbfc-linux

Starting from Fedora 38, [nbfc-linux](https://github.com/nbfc-linux/nbfc-linux) will only use the `ec_sys` module instead of `acpi_ec`. This means that if you're using nbfc-linux on your system, you'll need to have the `ec_sys` module installed.

## Installation

Download and extract this project. Then, navigate to its root directory in your terminal and run the following command for installation:

```
sudo make dkmsinstall
```

This command will automatically compile and build a module named `ec-sys.ko`, adding it to DKMS. Whenever there's an update of your system's kernel version, DKMS will rebuild and deploy that module file automatically.

## Uninstallation

If you want to uninstall this module file from your system, you can run the following command in its root directory:

```
sudo make dkmsuninstall
```


## Links 

Forked from [junocomputers/deb:ec-sys-dkms](https://gemfury.com/junocomputers/deb:ec-sys-dkms)

GitLab repository for `ec_sys-dkms`: [junocomputers/ec-sys-dkms](https://gitlab.com/junocomputers/ec-sys-dkms)